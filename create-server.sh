#!/bin/bash
set -e
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
if [[ -f "$DIR/.env" ]]; then
    source "$DIR/.env"
fi

SERVER_NAME="${SERVER_NAME:-$1}"
NETWORK_ADDRESS="${NETWORK_ADDRESS:-$2}"

if [[ "$SERVER_NAME" == '' ]]; then
    echo "Use: $0 <server_name> [network_mask_address]"
    exit
fi

if [[ "$(id -u)" != '0' ]]; then
    sudo bash $0 $@
    exit
fi

if [[ -d "$SERVER_DIR" ]]; then
    echo "[ERROR] Directory '$SERVER_DIR' already exists"
    exit 1
fi

################################################################################################
# VPN settings
OVPN_DIR="${OVPN_DIR:-/etc/openvpn}"
VPN_SERVER_FQN="${VPN_SERVER_FQN:-my.vpn.net}"

# SSL Settings
DH_KEY_SIZE="${DH_KEY_SIZE:-4096}"
SSL_KEY_SIZE="${SSL_KEY_SIZE:-4096}"
SSL_DAYS="${SSL_DAYS:-3650}"
SSL_CIPHER="${SSL_CIPHER:-AES-256-GCM}"
VPN_PORT_MIN=1194
VPN_PORT_MAX=1199

# Certificate settings
SSL_SUBJ_COUNTRY="${SSL_SUBJ_COUNTRY:-US}"
SSL_SUBJ_STATE="${SSL_SUBJ_STATE:-California}"
SSL_SUBJ_LOCATION="${SSL_SUBJ_LOCATION:-San Francisco}"
SSL_SUBJ_ORG="${SSL_SUBJ_ORG:-Unknown Company}"
SSL_SUBJ_OU="${SSL_SUBJ_OU:-Unknown Department}"
SSL_SUBJ_BASE="/C=$SSL_SUBJ_COUNTRY/ST=$SSL_SUBJ_STATE/L=$SSL_SUBJ_LOCATION/O=$SSL_SUBJ_ORG/OU=$SSL_SUBJ_OU"

# Certificate paths
SERVER_DIR="$OVPN_DIR/server_$SERVER_NAME"
SERVER_CONFIG="$OVPN_DIR/${SERVER_NAME}.conf"
CLIENT_TEMPLATE=$SERVER_DIR/client.conf.template
SSL_CA_CERT="$SERVER_DIR/ca.crt"
SSL_CA_KEY="$SERVER_DIR/ca.key"
SSL_SERVER_CERT="$SERVER_DIR/server.crt"
SSL_SERVER_KEY="$SERVER_DIR/server.key"
SSL_SERVER_CSR="$SERVER_DIR/csr/server.csr"
SSL_SERVER_CNF="$SERVER_DIR/server.cnf"
SSL_TLS_SECRET="$SERVER_DIR/ta.key"
SSL_DH_CERT="$SERVER_DIR/dh${DH_KEY_SIZE}.pem"
################################################################################################

#
# Get server port
#
if [[ "$SERVER_PORT" == '' ]]; then
    tmp="/tmp/ovpn.busyports.tmp"
    grep port "$OVPN_DIR/"*.conf 2>/dev/null | sed -r 's/^.*\b([0-9]+)\b/\1/g' > "$tmp"
    for port in $(seq $VPN_PORT_MIN $VPN_PORT_MAX); do
        if ! grep "$port" "$tmp" > /dev/null; then
            SERVER_PORT="$port"
            echo "[INFO] Found free port: $SERVER_PORT"
            break
        fi
    done
    if [[ "$SERVER_PORT" == '' ]]; then
        echo "[ERROR] Fail to found free port"
        exit 1
    fi
fi

#
# Get network address
#
if [[ "$NETWORK_ADDRESS" == '' ]]; then
    net_bit=$((SERVER_PORT - VPN_PORT_MIN))
    if [[ "$net_bit" == '' ]] || [[ $net_bit -lt 0 ]]; then
            net_bit=0
    fi

    NETWORK_ADDRESS="${NETWORK_ADDRESS:-10.$net_bit.0.0}"
fi
echo "[INFO] Network adress: $NETWORK_ADDRESS"

#
# Prepare folders
#
echo "[INFO] Creating server '$SERVER_NAME' directories"
mkdir -p $SERVER_DIR/{conf,logs,ccd,csr,clients}

#
# Client config template
#
ccd_addr=$(echo $NETWORK_ADDRESS | sed 's/\.0$//g')
echo "[INFO] Creating client config template"
cat << EOF > $SERVER_DIR/ccd/template
# Filename is equal to client name
# Static IP: ifconfig-push server_addr client_addr
# Doc: https://openvpn.net/community-resources/how-to/#configuring-client-specific-rules-and-access-policies
#
# ifconfig-push $ccd_addr.1 $ccd_addr.2
EOF

#
# Server certificates
#
echo "[INFO] Create a SSL server certificate config"
cat << EOF > "$SSL_SERVER_CNF"
[server_cert]
keyUsage = digitalSignature,keyEncipherment
extendedKeyUsage = serverAuth
EOF

echo "[INFO] Build a root certificate-key pair (CA)"
openssl req -nodes -new -x509 -keyout "$SSL_CA_KEY" -out "$SSL_CA_CERT" -subj "$SSL_SUBJ_BASE/CN=OpenVPN $SERVER_NAME CA" -days "$SSL_DAYS"

echo "[INFO] Build a server certificate (CRT, CSR, KEY)"
openssl genrsa -out "$SSL_SERVER_KEY" "$SSL_KEY_SIZE"
openssl req -new -key $SSL_SERVER_KEY -out "$SSL_SERVER_CSR" -subj "$SSL_SUBJ_BASE/CN=OpenVPN $SERVER_NAME Server"
openssl x509 -req -in "$SSL_SERVER_CSR" -CA "$SSL_CA_CERT" -CAkey "$SSL_CA_KEY" -CAcreateserial -out $SSL_SERVER_CERT -days "$SSL_DAYS" -extensions server_cert -extfile "$SSL_SERVER_CNF"

echo "[INFO] Generate a Diffie-Hellman key (may take a while)"
time openssl dhparam -out "$SSL_DH_CERT" $DH_KEY_SIZE

echo "[INFO] Generate a TLS key"
openvpn --genkey secret "$SSL_TLS_SECRET"

#
# Server config
#
echo "[INFO] Generate server config template"
cat << EOF > "$SERVER_CONFIG"
# Server settings
server $NETWORK_ADDRESS 255.255.255.0
port $SERVER_PORT
proto udp4
dev tun
persist-key
persist-tun

# Certificate settings
ca    $SSL_CA_CERT
cert  $SSL_SERVER_CERT
key   $SSL_SERVER_KEY
dh    $SSL_DH_CERT
cipher $SSL_CIPHER
auth SHA256
tls-auth $SSL_TLS_SECRET 0

# Network settings
topology subnet
ifconfig-pool-persist $SERVER_DIR/ipp.txt
client-config-dir $SERVER_DIR/ccd
max-clients 100
keepalive 10 120
explicit-exit-notify 1
#comp-lzo
#client-to-client
#push "route $NETWORK_ADDRESS 255.255.255.0"

# Logging settings
verb 3
mute 20
status $SERVER_DIR/logs/status.log
log $SERVER_DIR/logs/client.log
log-append $SERVER_DIR/logs/client.log
EOF


echo "[INFO] Generate client config template: $CLIENT_TEMPLATE"
cat << EOF > "$CLIENT_TEMPLATE"
# Client settings
client
remote $VPN_SERVER_FQN $SERVER_PORT
proto udp4
dev tun
nobind
persist-key
persist-tun
resolv-retry infinite

# Certificate settings
cipher $SSL_CIPHER
auth SHA256
remote-cert-tls server
key-direction 1

# Logging settings
verb 4
mute 20
EOF

#
# Starting service
#
if [[ "$(id -u)" == '0' ]]; then
    echo "[INFO] Installing service"
    systemctl enable openvpn@$SERVER_NAME
    systemctl restart openvpn@$SERVER_NAME
    systemctl status openvpn@$SERVER_NAME
fi

#
# Finish
#
echo "[INFO] Server created: $SERVER_CONFIG"
