#!/bin/bash
set -e
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
if [[ -f "$DIR/.env" ]]; then
    source "$DIR/.env"
fi

SERVER_NAME="$1"
CLIENT_NAME="$2"

OVPN_DIR="${OVPN_DIR:-/etc/openvpn}"
SERVER_DIR="$OVPN_DIR/server_$SERVER_NAME"
CLIENT_DIR="$SERVER_DIR/client_$CLIENT_NAME"

if [[ $# -lt 2 ]]; then
    echo "Sintax: $0 <SERVER_NAME> <CLIENT_NAME>"
    echo "    SERVER_NAME - SSL Company name (without preffix)"
    echo "    CLIENT_NAME - Name (login) of client"
    exit
fi

if [[ "$(id -u)" != '0' ]]; then
    sudo bash $0 $@
    exit
fi

if [[ $(echo -n "$CLIENT_NAME" | wc -m) -gt 64 ]]; then
    echo "[ERROR] Client name cannot be more than 64 characters"
fi

if [[ ! -d "$SERVER_DIR" ]]; then
    echo "[ERROR] Directory '$SERVER_DIR' does not exists"
    exit 1
fi

################################################################################################
# VPN settings
VPN_SERVER_FQN="${VPN_SERVER_FQN:-my.vpn.net}"

# SSL Settings
SSL_KEY_SIZE="${SSL_KEY_SIZE:-4096}"
SSL_DAYS="${SSL_DAYS:-3650}"
SSL_CIPHER="${SSL_CIPHER:-AES-256-GCM}"

# Certificate settings
SSL_SUBJ_COUNTRY="${SSL_SUBJ_COUNTRY:-US}"
SSL_SUBJ_STATE="${SSL_SUBJ_STATE:-California}"
SSL_SUBJ_LOCATION="${SSL_SUBJ_LOCATION:-San Francisco}"
SSL_SUBJ_ORG="${SSL_SUBJ_ORG:-Unknown Company}"
SSL_SUBJ_OU="${SSL_SUBJ_OU:-Unknown Department}"
SSL_SUBJ_BASE="/C=$SSL_SUBJ_COUNTRY/ST=$SSL_SUBJ_STATE/L=$SSL_SUBJ_LOCATION/O=$SSL_SUBJ_ORG/OU=$SSL_SUBJ_OU"

# Server certificates
SSL_CA_CERT="$SERVER_DIR/ca.crt"
SSL_CA_KEY="$SERVER_DIR/ca.key"
SSL_TLS_SECRET="$SERVER_DIR/ta.key"

# Client certificate paths (based on CLIENT_DIR=/etc/openvpn/server_$$/client_$$)
SSL_CLIENT_CERT="$CLIENT_DIR/$CLIENT_NAME.crt"
SSL_CLIENT_KEY="$CLIENT_DIR/$CLIENT_NAME.key"
SSL_CLIENT_CSR="$SERVER_DIR/csr/$CLIENT_NAME.csr"

# Other paths
CLIENT_TEMPLATE="$SERVER_DIR/client.conf.template"
CLIENT_CONFIG="$CLIENT_DIR/${SERVER_NAME}_$CLIENT_NAME.conf"
CLIENT_LOG_PATH_BASE="$OVPN_DIR/${SERVER_NAME}_$CLIENT_NAME"
CLIENT_ARCHIVE="$SERVER_DIR/clients/${SERVER_NAME}_${CLIENT_NAME}.zip"

################################################################################################

if [[ -d "$CLIENT_DIR" || -f "$CLIENT_ARCHIVE" ]]; then
    echo "[ERROR] Client '$CLIENT_NAME' already exist"
    exit 1
fi

#
# Prepare folders
#
echo "[INFO] Creating client '$CLIENT_DIR' directories"
mkdir -p $CLIENT_DIR/{$SERVER_NAME,${SERVER_NAME}_$CLIENT_NAME}

#
# Client certificates
#
echo "[INFO] Build a client '$CLIENT_NAME' certificate (CRT, CSR, KEY)"
openssl genrsa -out "$SSL_CLIENT_KEY" "$SSL_KEY_SIZE"
openssl req -new -key "$SSL_CLIENT_KEY" -out "$SSL_CLIENT_CSR" -subj "$SSL_SUBJ_BASE/CN=$CLIENT_NAME"
openssl x509 -req -in "$SSL_CLIENT_CSR" -CA "$SSL_CA_CERT" -CAkey "$SSL_CA_KEY" -CAcreateserial -out "$SSL_CLIENT_CERT" -days "$SSL_DAYS"

#
# Client config
#
echo "[INFO] Generate client config"
cat << EOF > "$CLIENT_CONFIG"
$(cat $CLIENT_TEMPLATE)
#status ${CLIENT_LOG_PATH_BASE}.status.log
#log ${CLIENT_LOG_PATH_BASE}.client.log

# Certificates content
<ca>
$(cat $SSL_CA_CERT)
</ca>

<cert>
$(cat $SSL_CLIENT_CERT)
</cert>

<key>
$(cat $SSL_CLIENT_KEY)
</key>

<tls-auth>
$(cat $SSL_TLS_SECRET)
</tls-auth>
EOF

#
# Archive
#
echo "[INFO] Making client archive"
cd "$CLIENT_DIR"
zip -r "$CLIENT_ARCHIVE" "$(basename $CLIENT_CONFIG)"

#
# Cleanup
#
echo "[INFO] Cleanup"
rm -rf "$SERVER_DIR/client_$CLIENT_NAME"

#
# Finish
#
echo "[INFO] Done: $CLIENT_ARCHIVE"
